//
//  PockerView.m
//  TarotTest
//
//  Created by kpb on 2021/6/4.
//  Copyright © 2021 motic. All rights reserved.
//

#import "PockerView.h"

@implementation PockerView

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.isOpen = false;
        
        //设置阴影
        self.layer.shadowColor = [UIColor blackColor].CGColor;
        self.layer.shadowOffset = CGSizeMake(-10, 0);
        self.layer.shadowOpacity = 0.3;
    }
    return self;
}

-(void)addPocker:(NSString *)imageName {
    //牌的正面
    self.imgView2 = [[UIImageView alloc] init];
    [self addSubview:self.imgView2];
    [self.imgView2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.bottom.right.equalTo(self);
    }];
    self.imgView2.backgroundColor = [UIColor redColor];
    self.imgView2.image = [UIImage imageNamed:imageName];
    self.imgView2.layer.cornerRadius = 10;
    self.imgView2.clipsToBounds = YES;
    self.imgView2.layer.borderWidth = 5;
    self.imgView2.layer.borderColor = [[UIColor whiteColor] CGColor];
    
    //牌的背面
    self.imgView1 = [[UIImageView alloc] init];
    [self addSubview:self.imgView1];
    [self.imgView1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.bottom.right.equalTo(self);
    }];
    self.imgView1.backgroundColor = [UIColor redColor];
    self.imgView1.image = [UIImage imageNamed:@"1.jpeg"];
    self.imgView1.layer.cornerRadius = 10;
    self.imgView1.clipsToBounds = YES;
    self.imgView1.layer.borderWidth = 5;
    self.imgView1.layer.borderColor = [UIColor whiteColor].CGColor;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        
    }
    return self;
}



@end
